export class Booking {
  public bookingId: number;
  public bookingPrice: string;
  public bookingTime: string;
  public tutenUserClient: TutenUserClient;
  public locationId: LocationId;
}

class TutenUserClient {
  public firstname: string;
  public lastname: string;
}

class LocationId {
  public streetAddress: string;
}