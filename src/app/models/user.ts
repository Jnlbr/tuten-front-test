export class User {
  public serviceData: any;
  public userAvailability: any;
  public sessionTokenBck: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public active: boolean;
  public password: string;
  public passwordHash: any;
  public sessionTokenWeb: any;
  public phoneNumber: string;
  public agreedToTermsOfUse: true;
  public whereKnownUs: any;
  public lastLogin: string;
  public sessionTokenCli: any;
  public sessionTokenPro: any;
  public funds: number;
  public tokenFacebook: any;
  public tokenGoogle: any;
  public tokensIonic: any;
  public photoPath: any;
  public photoExt: any;
  public userRole: UserRole;
  public sync: number;
  public usedCodeList: string;
  public referrer: string;
  public rut: any;
  public domain: string;
  public typeProfessional: any;
  public tutenSubRole: any;
  public userId: any;
  public appVersion: any;
  public estatus: any
}

export class UserRole {
  public userRole: string;
  public description: string;
  public fatherUserRole: any;
  public domain: string;
  public estatus: any;
  public defaultNamespace: any;
  public id: number;
}