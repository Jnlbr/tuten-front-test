import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { User } from '../models/user';
import { Booking } from '../models/booking';

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  private url = 'https://dev.tuten.cl/TutenREST/rest/user/contacto@tuten.cl/bookings';
  // private email = 'contacto@tuten.cl';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }


  public getBookings(current: boolean = true): Observable<Booking[]> {
    const user: User = this.authService.getUser();
    return this.http.get<Booking[]>(`${this.url}?current=${current}`, {
      headers: {
        Accept: 'application/json',
        Token: user.sessionTokenBck,
        Adminemail: user.email,
        App: 'APP_BCK'
      }
    });
  }
}
