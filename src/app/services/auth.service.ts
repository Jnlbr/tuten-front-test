import { Injectable } from '@angular/core';
import { Observable, pipe } from 'rxjs';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = 'https://dev.tuten.cl:443/TutenREST/rest/user';
  public loggedIn = false;
  private user: User = null;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  public logIn(user: User): Observable<User> {
    return this.http.put<User>(`${this.url}/${user.email}`, {}, {
      headers: {
        App: 'APP_BCK',
        Password: user.password,
        Accept: 'application/json'
      }
    })
  }
  public logOut(): void {
    this.user = null;
    this.loggedIn = false;
    localStorage.clear();
    this.router.navigate(['/log-in']);
  }


  public setUser(user: User): void {
    this.user = user;
    this.loggedIn = true;
    localStorage.setItem('user', JSON.stringify(user));
  }
  public getUser(): User {
    if (!this.user) {
      const user: User = JSON.parse(localStorage.getItem('user'));
      this.user = user || null;
      this.loggedIn = user ? true : false;
    }
    return this.user;
  }
}
