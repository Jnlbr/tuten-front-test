import { Component, OnInit } from "@angular/core";
import { BookingService } from "../services/booking.service";
import { Booking } from "../models/booking";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  public allBookings: Booking[];
  public bookings: Booking[];

  public byId: number;
  public byPrice: string;
  public priceFocus = false;

  public priceCriteria = "=";

  constructor(private bookingService: BookingService) {}

  ngOnInit(): void {
    this.bookingService.getBookings().subscribe((bookings: Booking[]) => {
      this.allBookings = bookings;
      this.bookings = bookings;
    });
  }

  public searchById(): void {
    this.bookings = this.byId
      ? this.allBookings.filter(booking => booking.bookingId === this.byId)
      : this.allBookings;
  }

  public searchByPrice(): void {
    // this.bookings = this.byPrice ? this.allBookings.filter(booking => booking.bookingPrice === this.byPrice) : this.allBookings;
    if (this.byPrice) {
      switch (this.priceCriteria) {
        case "=":
          this.bookings = this.allBookings.filter(
            booking => booking.bookingPrice === this.byPrice
          );
          break;
        case "<=":
          this.bookings = this.allBookings.filter(
            booking => booking.bookingPrice <= this.byPrice
          );
          break;
        case ">=":
          this.bookings = this.allBookings.filter(
            booking => booking.bookingPrice >= this.byPrice
          );
          break;
      }
    }
  }

  // Events
  public onPriceFocus(): void {
    this.priceFocus = true;
  }
  public onPriceFocusOut(): void {
    this.priceFocus = false;
  }
  public onPriceCriteriaSelect(criteria: string): void {
    this.priceCriteria = criteria;
    this.searchByPrice();
  }
}
