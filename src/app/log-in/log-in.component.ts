import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  public loading = false;
  public user: User = new User();

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }


  public logIn(): void {
    this.loading = true;
    this.authService.logIn(this.user)
      .subscribe((user: User) => {
        this.loading = false;
        this.authService.setUser(user);
        this.router.navigate(['/']);
      });
  }
}
